
<!-- Footer Start -->
<footer class="site-footer footer-style-1">

    <div class="footer-top-pic">
        <img src="images/footer-dot-pic-2.png" alt="javascript:;">
    </div>
    <div class="footer-bottom-pic">
        <img src="images/footer-dot-pic-2.png" alt="javascript:;">
    </div>

    <!-- FOOTER BLOCKES START -->  
    <div class="footer-top">
        <div class="container">                      

            <div class="row">
                <!-- COLUMNS 1 -->
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="sf-widget-link f-margin d-flex flex-column align-items-center">
                        <div class="aon-footer-logo-2 ">
                            <img src="images/logo.png" alt="">
                        </div>
                        <p class="text-center">PRIMA PLM specializes in the implementation and operation of Teamcenter and CAD tools. With many years of experience, we offer expert consulting on engineering and manufacturing processes, ensuring their effective mapping and implementation in Teamcenter.</p>
                    </div>
                </div>
                <!-- COLUMNS 2 -->
                <div class="col-lg-2 col-md-6 col-12">
                    <div class="aon-widget-link f-margin">
                        <h4 class="aon-f-title-2">Quick Links</h4>
                        <ul class="aon-ftr-info">
                            <li><a href="https://primaplm.com/"> Home </a></li>
                            <li><a href="about-us"> About Us </a></li>
                            <li><a href="our-services"> Our Services </a></li>
                            <li><a href="contact-us"> Contact Us </a></li>
                        </ul>
                    </div>
                </div>
                <!-- COLUMNS 3 -->
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="aon-ftr-info-wrap  f-margin">
                        <h4 class="aon-f-title-2">Services</h4>
                        <ul class="aon-widget-foo-list-2">
                            <li><span> Business Consulting </span></li>
                            <li><span> Teamcenter solutions </span></li>
                            <li><span> Data Migration </span></li>
                            <li><span> Integration and solutions </span></li>
                            <li><span> Installation and Upgrade</span></li>
                            <li><span> App Management & Support </span></li>
                        </ul>
                    </div>
                </div>
                <!-- COLUMNS 4 -->
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="aon-ftr-info-wrap  f-margin">
                        <h4 class="aon-f-title-2">Connect With US</h4>
                        <ul class="aon-ftr-info mb-4">
                            <li><i class="flaticon-084-email"></i> <span>info@primaplm.com</span></li>
                        </ul>
<!--                        <div class="aon-live-chats">
                            <div class="aon-live-icon">
                                <i class="flaticon-003-chat-1"></i>
                            </div>
                            <div class="aon-live-text">
                                <a href="contact-us.php">
                                    <span>Need Help Now?</span>
                                    <strong>Chat with Us!</strong>
                                </a>
                            </div>
                        </div>-->
                    </div>
                </div>

            </div>
            <div class="text-center">
                <div class="ftr-copyright">© Copyright <script>document.write(new Date().getFullYear());</script>  <span>PRIMA PLM SOLUTIONS - </span> All Rights Reserved</div>
            </div>
        </div>
    </div>

</footer>
<!-- Footer End -->	