<!DOCTYPE html>
<html lang="zxx">

    <head>
        <!-- META -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">	

        <title>Prima PLM Solutions - Technology Consulting and Implementation</title>
        <meta name="description" content="Prima PLM Solutions provides expert technology consulting, implementation services, and PLM solutions to help your business succeed across Europe, the USA, and APAC.">
        <meta name="keywords" content="PLM solutions, technology consulting, implementation services, Teamcenter solutions, business consulting, data migration, application management, Europe, USA, APAC">
        <meta name="author" content="Prima PLM Solutions">

        <!-- Open Graph / Facebook -->
        <meta property="og:type" content="website">
        <meta property="og:url" content="https://www.primaplm.com">
        <meta property="og:title" content="Prima PLM Solutions - Technology Consulting and Implementation">
        <meta property="og:description" content="Prima PLM Solutions offers expert technology consulting, implementation services, and PLM solutions to help your business succeed across Europe, the USA, and APAC.">
        <meta property="og:image" content="https://www.primaplm.com/images/slider-home-1/bg-1.jpg">

        <!-- Twitter -->
        <meta property="twitter:card" content="summary_large_image">
        <meta property="twitter:url" content="https://www.primaplm.com">
        <meta property="twitter:title" content="Prima PLM Solutions - Technology Consulting and Implementation">
        <meta property="twitter:description" content="Prima PLM Solutions provides expert technology consulting, implementation services, and PLM solutions to help your business succeed across Europe, the USA, and APAC.">
        <meta property="twitter:image" content="https://www.primaplm.com/images/slider-home-1/bg-1.jpg">

        <!-- Organization schema -->
        <script type="application/ld+json">
            {
            "@context": "https://schema.org",
            "@type": "Organization",
            "name": "Prima PLM Solutions",
            "url": "https://www.primaplm.com",
            "logo": "https://www.primaplm.com/images/logo.png",
            "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "+1-800-555-1234",
            "contactType": "Customer Service"
            },
            "sameAs": [
            "https://www.facebook.com/yourprofile",
            "https://www.twitter.com/yourprofile",
            "https://www.linkedin.com/company/yourprofile"
            ]
            }
        </script>

        <!-- Canonical URL -->
        <link rel="canonical" href="https://www.primaplm.com">
        <!-- Robots Meta Tag -->
        <meta name="robots" content="index, follow">
        <!-- Author Tag -->
        <meta name="author" content="Prima PLM Solutions">

        <!--FAVICON-->
        <link rel="apple-touch-icon" sizes="180x180" href="./images/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="./images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon/favicon-16x16.png">
        <link rel="manifest" href="./images/favicon/site.webmanifest">

        <!-- BOOTSTRAP STYLE SHEET -->
        <link  href="css/bootstrap.min.css" rel="stylesheet">
        <!-- FONTAWESOME STYLE SHEET -->
        <link  href="css/font-awesome.min.css" rel="stylesheet">
        <!-- Feather STYLE SHEET -->
        <link href="css/feather.css" rel="stylesheet">
        <!-- FLATICON STYLE SHEET -->
        <link href="css/flaticon.min.css" rel="stylesheet">
        <!-- WOW ANIMATE STYLE SHEET -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- FONTS CSS STYLE SHEET -->
        <link href="css/font.css" rel="stylesheet">
        <!-- OWL CAROUSEL STYLE SHEET -->
        <link href="css/owl.carousel.min.css" rel="stylesheet">
        <!-- MAGNIFIC POPUP STYLE SHEET -->
        <link href="css/magnific-popup.min.css" rel="stylesheet">     
        <!-- LC LIGHT BOX STYLE SHEET -->
        <link href="css/lc_lightbox.css" rel="stylesheet">   
        <!-- MAIN STYLE SHEET -->
        <link href="css/style.css" rel="stylesheet">

    </head>

    <body>

        <!-- LOADING AREA START ===== -->
        <div class="loading-area" style="display: none;">
            <div class="loading-box"></div>
            <div class="loading-pic">
                <div class="loadind-box" >
                    <span class="let1">l</span>  
                    <span class="let2">o</span>  
                    <span class="let3">a</span>  
                    <span class="let4">d</span>  
                    <span class="let5">i</span>  
                    <span class="let6">n</span>  
                    <span class="let7">g</span>  
                </div>
            </div>
        </div>
        <!-- LOADING AREA  END ====== -->

        <div class="page-wraper">

            <?php include_once './i-header.php'; ?>

            <!-- CONTENT START -->
            <div class="page-content">

                <!-- Slider 1  -->
                <div class="aon-bnr-carousal-area">
                    <!--Section Content Start-->
                    <div class="section-content">
                        <div class="owl-carousel aon-bnr1-carousal owl-btn-vertical-center">
                            <!--block 1-->
                            <div class="item">
                                <div class="aon-slider1-wrap" style="background-image:url(images/slider-home-1/bg-1.jpg)">
                                    <div class="overlay"></div>
                                    <div class="container">
                                        <div class="aon-slider1-content">
                                            <div class="aon-slider1-title">PLM Consulting & Implementation</div>
                                            <div class="aon-slider1-detail"> Prima PLM Solutions provides consulting and implementation services across Europe, the USA, and APAC. We take pride in our highly professional and service-focused approach within the PLM domain.</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--block 2-->
                            <div class="item">
                                <div class="aon-slider1-wrap" style="background-image:url(images/slider-home-1/bg-2.jpg)">
                                    <div class="overlay"></div>
                                    <div class="container">
                                        <div class="aon-slider1-content">
                                            <div class="aon-slider1-title">Your Long-Term Partner in PLM</div>
                                            <div class="aon-slider1-detail"> Our unique value proposition—being better, faster, and more cost-effective—resonates with our clients, many of whom see us as a long-term partner in achieving their business objectives. Our team collaborates closely with you to develop solutions tailored to your specific requirements, ensuring optimal outcomes for your projects.</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Section Content End-->
                </div>
                <!-- Slider 1 End --> 

                <!-- About Section -->
                <div class="aon-about-comp-area p-t120 p-b80 aon-bg-gray">
                    <div class="container">
                        <div class="section-content">
                            <div class="row align-items-center">
                                <!-- Column 1 -->
                                <div class="col-lg-6 wow fadeInDown" data-wow-duration="1000ms">
                                    <div class="aon-product-left">
                                        <div class="aon-pro-pic1">
                                            <img src="images/pro-pic2.jpg" alt="">
                                        </div>
                                        <div class="aon-pro-pic2">
                                            <img src="images/pro-pic1.jpg" alt="">
                                        </div>
                                        <div class="aon-pro-pic-circle animate-v">
                                            <div class="aon-pro-pic-num">
                                                <span>Global <br>Presence</span>
                                            </div>
                                            <div class="aon-pro-circle-animate"></div>
                                        </div>
                                        <span class="aon-pro-pic-label">Serving Europe, USA, & APAC</span>
                                    </div>
                                </div>
                                <!-- Column 2 -->
                                <div class="col-lg-6 wow fadeInDown" data-wow-duration="2000ms">
                                    <div class="aon-about-comp-right">
                                        <span class="aon-sub-title">About Company</span>
                                        <h2 class="aon-pro-title">Accelerating Your Business Through Innovative PLM Solutions</h2>
                                        <div class="aon-about-comp m-b30">
                                            <p>Prima PLM Solutions provides technology consulting and implementation services across Europe, the USA, and APAC. We take pride in our highly professional and service-focused approach within the PLM domain.</p>
                                            <p>Our unique value proposition—being better, faster, and more cost-effective—resonates with our clients, many of whom see us as a long-term partner in achieving their business objectives. Our team collaborates closely with you to develop solutions tailored to your specific requirements, ensuring optimal outcomes for your projects.</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-7 col-md-6 m-b30">
                                                <div class="aon-abo-com-box">
                                                    <p>Our approach is professional and service-focused, ensuring that we meet your specific needs efficiently.</p>
                                                </div>

                                            </div>
                                            <div class="col-lg-5 col-md-6 m-b30">
                                                <div class="aon-pricing-table-bottom">
                                                    <a class="site-button site-btn-curve" href="about-us">Read More</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="line-gray-botton"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- About Section End -->  

                <!--Info Section-->
                <section class="aon-static-area p-t50 p-b50 aon-bg-dark">
                    <div class="container">
                        <div class="aon-features-bot-area p-t80">
                            <div class="row justify-content-center">
                                <!--block 1-->
                                <div class="col-lg-4 col-md-6 col-sm-12 m-b30 aon-feas-wrap-col">
                                    <div class="aon-icon-effect  wow fadeInDown" data-wow-duration="2000ms">
                                        <div class="aon-feas-col">
                                            <div class="aon-feas-col-icon"> 
                                                <i class="flaticon-018-rocket"></i>
                                            </div>
                                            <div class="aon-feas-col-text d-flex flex-column">
                                                <h4 class="aon-feas-col-title text-white">Strategic Planning</h4>
                                                <p class="text-white">Guiding your journey towards success.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--block 2-->
                                <div class="col-lg-4 col-md-6 col-sm-12 m-b30 aon-feas-wrap-col">
                                    <div class="aon-icon-effect  wow fadeInDown" data-wow-duration="2000ms">
                                        <div class="aon-feas-col">
                                            <div class="aon-feas-col-icon"> 
                                                <i class="flaticon-022-mouse"></i>
                                            </div>
                                            <div class="aon-feas-col-text d-flex flex-column">
                                                <h4 class="aon-feas-col-title text-white">Tailored Solutions</h4>
                                                <p class="text-white">Customized services for your unique needs.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--block 2-->
                                <div class="col-lg-4 col-md-6 col-sm-12 m-b30 aon-feas-wrap-col">
                                    <div class="aon-icon-effect  wow fadeInDown" data-wow-duration="2000ms">
                                        <div class="aon-feas-col">
                                            <div class="aon-feas-col-icon"> 
                                                <i class="flaticon-028-joystick-1"></i>
                                            </div>
                                            <div class="aon-feas-col-text d-flex flex-column">
                                                <h4 class="aon-feas-col-title text-white">Optimized Performance</h4>
                                                <p class="text-white">Maximizing efficiency and results.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Info Section End-->

                <!-- Our Services Section -->
                <div class="aon-features2-area p-t100 p-b100 aon-bg-white">
                    <div class="features2-bg-top"> </div>
                    <div class="container">
                        <div class="section-content">
                            <!--Title Section Start-->
                            <div class="section-head center">
                                <span class="aon-sub-title">OUR SERVICES LIST</span>
                                <h2 class="aon-title">The Services We Are Offering.</h2>
                            </div>
                            <!--Title Section End-->
                            <div class="row">

                                <div class="col-xl-4 col-lg-4 col-md-6 m-b30">
                                    <div class="aon-feas-box-wrap shadow aon-icon-effect  wow fadeInDown" data-wow-duration="2000ms">
                                        <div class="aon-feas-box m-0">
                                            <div class="aon-feas-icon"> 
                                                <img class="aon-ico" src="images/featuers-icon/pic1.png" alt="" >
                                            </div>
                                            <h4 class="aon-feas-title">Business Consulting</h4>
                                            <div class="aon-feas-text">Expert guidance to help you make strategic business decisions and drive your company forward.</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-6 m-b30">
                                    <div class="aon-feas-box-wrap shadow aon-icon-effect  wow fadeInDown" data-wow-duration="2000ms">
                                        <div class="aon-feas-box m-0">
                                            <div class="aon-feas-icon"> 
                                                <img class="aon-ico" src="images/featuers-icon/pic2.png" alt="" >
                                            </div>
                                            <h4 class="aon-feas-title">Teamcenter solutions </h4>
                                            <div class="aon-feas-text">Tailored implementation and configuration of Teamcenter to meet your specific business needs.</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-6 m-b30">
                                    <div class="aon-feas-box-wrap shadow aon-icon-effect  wow fadeInDown" data-wow-duration="2000ms">
                                        <div class="aon-feas-box m-0">
                                            <div class="aon-feas-icon"> 
                                                <img class="aon-ico" src="images/featuers-icon/pic6.png" alt="" >
                                            </div>
                                            <h4 class="aon-feas-title">Data Migration</h4>
                                            <div class="aon-feas-text">Facilitating a secure and seamless transition of your data with minimal disruption to your business.</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-6 m-b30">
                                    <div class="aon-feas-box-wrap shadow aon-icon-effect  wow fadeInDown" data-wow-duration="2000ms">
                                        <div class="aon-feas-box m-0">
                                            <div class="aon-feas-icon"> 
                                                <img class="aon-ico" src="images/featuers-icon/pic3.png" alt="" >
                                            </div>
                                            <h4 class="aon-feas-title">Integration and solutions </h4>
                                            <div class="aon-feas-text">Offering seamless integration services and innovative solutions to enhance your business processes.</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-6 m-b30">
                                    <div class="aon-feas-box-wrap shadow aon-icon-effect  wow fadeInDown" data-wow-duration="2000ms">
                                        <div class="aon-feas-box m-0">
                                            <div class="aon-feas-icon"> 
                                                <img class="aon-ico" src="images/featuers-icon/pic4.png" alt="" >
                                            </div>
                                            <h4 class="aon-feas-title">Installation and Upgrade</h4>
                                            <div class="aon-feas-text">Efficient installation and seamless upgrades to ensure your systems are up-to-date and running smoothly.</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-6 m-b30">
                                    <div class="aon-feas-box-wrap shadow aon-icon-effect  wow fadeInDown" data-wow-duration="2000ms">
                                        <div class="aon-feas-box m-0">
                                            <div class="aon-feas-icon"> 
                                                <img class="aon-ico" src="images/featuers-icon/pic5.png" alt="" >
                                            </div>
                                            <h4 class="aon-feas-title">App Management & Support</h4>
                                            <div class="aon-feas-text">Providing proactive management and reliable support to ensure the smooth functioning of your applications.</div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- Our Services Section End --> 

            </div>
            <!-- CONTENT END -->

            <?php include_once './i-footer.php'; ?>

            <!-- BUTTON TOP START -->
            <button class="scroltop"><span class="fa fa-angle-up  relative" id="btn-vibrate"></span></button>	


        </div>



        <!-- JAVASCRIPT  FILES ========================================= --> 
        <script  src="js/jquery-3.6.1.min.js"></script><!-- JQUERY.MIN JS -->
        <script  src="js/popper.min.js"></script><!-- POPPER.MIN JS -->
        <script  src="js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->
        <script  src="js/wow.js"></script><!-- WOW JS -->
        <script  src="js/jquery.bootstrap-touchspin.js"></script><!-- FORM JS -->
        <script  src="js/magnific-popup.min.js"></script><!-- MAGNIFIC-POPUP JS -->
        <script  src="js/isotope.pkgd.min.js"></script><!-- isotope-pkgd JS -->	
        <script  src="js/imagesloaded.pkgd.js"></script><!-- isotope-pkgd JS -->		
        <script  src="js/waypoints.min.js"></script><!-- WAYPOINTS JS -->
        <script  src="js/counterup.min.js"></script><!-- COUNTERUP JS -->
        <script  src="js/waypoints-sticky.min.js"></script><!-- STICKY HEADER -->
        <script  src="js/owl.carousel.min.js"></script><!-- OWL  SLIDER  -->
        <script  src="js/theia-sticky-sidebar.js"></script><!-- STICKY SIDEBAR  -->
        <script  src="js/lc_lightbox.lite.js" ></script><!-- IMAGE POPUP -->
        <script  src="js/custom.js"></script><!-- CUSTOM FUCTIONS  -->


    </body>

</html>




