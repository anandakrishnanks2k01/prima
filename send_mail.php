<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = htmlspecialchars($_POST['Name']);
    $company = htmlspecialchars($_POST['Company']);
    $email = htmlspecialchars($_POST['Email']);
    $phone = htmlspecialchars($_POST['Phone']);
    $city = htmlspecialchars($_POST['City']);
    $country = htmlspecialchars($_POST['Country']);
    $message = htmlspecialchars($_POST['message']);

    // Send email to the specified recipient
    $to = "merabtdeveloper@gmail.com";
    $subject = "Contact Form Submission from $name";
    $body = "
        <html>
        <head>
        <title>Contact Form Submission</title>
        </head>
        <body>
        <h2>Contact Form Submission</h2>
        <p><strong>Name:</strong> $name</p>
        <p><strong>Company:</strong> $company</p>
        <p><strong>Email:</strong> $email</p>
        <p><strong>Phone:</strong> $phone</p>
        <p><strong>City:</strong> $city</p>
        <p><strong>Country:</strong> $country</p>
        <p><strong>Message:</strong><br>$message</p>
        </body>
        </html>
    ";

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= "From: no-reply@yourdomain.com" . "\r\n"; // Changed the "From" header to a fixed email

    $send_to_recipient = mail($to, $subject, $body, $headers);

    // Send confirmation email to the sender
    $reply_subject = "Thank you for contacting us";
    $reply_body = "
        <html>
        <head>
        <title>Thank you for contacting us</title>
        </head>
        <body>
        <h2>Thank you, $name!</h2>
        <p>We have received your message and will get back to you shortly.</p>
        <p><strong>Your Message:</strong></p>
        <p>$message</p>
        <br>
        <p>Best regards,</p>
        <p>The Team</p>
        </body>
        </html>
    ";

    $reply_headers = "MIME-Version: 1.0" . "\r\n";
    $reply_headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $reply_headers .= "From: no-reply@yourdomain.com" . "\r\n";

    $send_to_sender = mail($email, $reply_subject, $reply_body, $reply_headers);

    if ($send_to_recipient && $send_to_sender) {
        http_response_code(200);
        echo "Message sent successfully!";
    } else {
        http_response_code(500);
        echo "Message could not be sent.";
    }
} else {
    http_response_code(403);
    echo "There was a problem with your submission, please try again.";
}
?>
