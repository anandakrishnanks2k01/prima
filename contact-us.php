
<!DOCTYPE html>
<html lang="zxx">

    <head>

        <!-- META -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <title>Contact Us - Prima PLM Solutions</title>
        <meta name="description" content="Get in touch with Prima PLM Solutions for expert consulting and implementation services. Contact us via phone, email, or visit our offices.">

        <!-- Open Graph / Facebook -->
        <meta property="og:type" content="website">
        <meta property="og:url" content="https://www.primaplm.com/contact-us">
        <meta property="og:title" content="Contact Us - Prima PLM Solutions">
        <meta property="og:description" content="Reach out to Prima PLM Solutions for expert consulting and implementation services. Contact us via phone, email, or visit our offices.">
        <meta property="og:image" content="https://www.primaplm.com/images/bg/contact_us.webp">

        <!-- Twitter -->
        <meta property="twitter:card" content="summary_large_image">
        <meta property="twitter:url" content="https://www.primaplm.com/contact-us">
        <meta property="twitter:title" content="Contact Us - Prima PLM Solutions">
        <meta property="twitter:description" content="Contact Prima PLM Solutions for expert consulting and implementation services. Reach us via phone, email, or visit our offices.">
        <meta property="twitter:image" content="https://www.primaplm.com/images/images/bg/contact_us.webp">

        <!-- Organization schema -->
        <script type="application/ld+json">
            {
            "@context": "https://schema.org",
            "@type": "Organization",
            "name": "Prima PLM Solutions",
            "url": "https://www.primaplm.com",
            "logo": "https://www.primaplm.com/images/logo.png",
            "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "+1-800-555-1234",
            "contactType": "Customer Service"
            },
            "sameAs": [
            "https://www.facebook.com/yourprofile",
            "https://www.twitter.com/yourprofile",
            "https://www.linkedin.com/company/yourprofile"
            ]
            }
        </script>

        <!-- Canonical URL -->
        <link rel="canonical" href="https://www.primaplm.com/contact-us">

        <!--FAVICON-->
        <link rel="apple-touch-icon" sizes="180x180" href="./images/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="./images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon/favicon-16x16.png">
        <link rel="manifest" href="./images/favicon/site.webmanifest">

        <!-- BOOTSTRAP STYLE SHEET -->
        <link  href="css/bootstrap.min.css" rel="stylesheet">
        <!-- FONTAWESOME STYLE SHEET -->
        <link  href="css/font-awesome.min.css" rel="stylesheet">
        <!-- Feather STYLE SHEET -->
        <link href="css/feather.css" rel="stylesheet">
        <!-- FLATICON STYLE SHEET -->
        <link href="css/flaticon.min.css" rel="stylesheet">
        <!-- WOW ANIMATE STYLE SHEET -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- FONTS CSS STYLE SHEET -->
        <link href="css/font.css" rel="stylesheet">
        <!-- OWL CAROUSEL STYLE SHEET -->
        <link href="css/owl.carousel.min.css" rel="stylesheet">
        <!-- MAGNIFIC POPUP STYLE SHEET -->
        <link href="css/magnific-popup.min.css" rel="stylesheet">     
        <!-- LC LIGHT BOX STYLE SHEET -->
        <link href="css/lc_lightbox.css" rel="stylesheet">   
        <!-- MAIN STYLE SHEET -->
        <link href="css/style.css" rel="stylesheet">
        <!-- Price Range Slider -->
        <link rel="stylesheet" href="css/bootstrap-slider.min.css"> 

    </head>

    <body>

        <!-- LOADING AREA START ===== -->
        <div class="loading-area" style="display: none;">
            <div class="loading-box"></div>
            <div class="loading-pic">
                <div class="loadind-box" >
                    <span class="let1">l</span>  
                    <span class="let2">o</span>  
                    <span class="let3">a</span>  
                    <span class="let4">d</span>  
                    <span class="let5">i</span>  
                    <span class="let6">n</span>  
                    <span class="let7">g</span>  
                </div>
            </div>
        </div>
        <!-- LOADING AREA  END ====== -->

        <div class="page-wraper">

            <?php include_once './i-header.php'; ?>

            <!-- Content -->
            <div class="page-content">

                <!-- Inner Page Banner  -->
                <div class="aon-inner-banner-area-contact p-b70">
                    <div class="aon-inner-banner-row">
                        <div class="container aon-inner-banner-container">
                            <div class="aon-inner-banner-title">Contact Us</div>
                        </div>
                    </div>
                </div>
                <!-- Inner Page Banner End --> 

                <!-- Product Section -->
                <div class="aon-shop-wrap p-t0 p-b70">
                    <div class="container">

                        <div class="aon-contact-info-area p-t50 p-b120 aon-bg-white">
                            <div class="container">
                                <div class="section-head center">
                                    <h2 class="aon-title">Contact Us</h2>
                                </div>
                                <div class="section-content">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6 col-md-12 m-b30">
                                            <div class="aon-con-info-box">
                                                <div class="aon-con-icon">
                                                    <i class="flaticon-090-pin"></i> 
                                                </div>
                                                <h4 class="aon-con-info-title">Contact Location</h4>
                                                <ul class="aon-con-info-list">
                                                    <li>
                                                        <div class="aon-con-text">
                                                            <strong>Prima PLM Solutions LTD<br>
                                                                Dept 5764, 196 High Road, Wood Green, London, N22 8HH, England</strong>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 m-b30">
                                            <div class="aon-con-info-box">
                                                <div class="aon-con-icon">
                                                    <i class="flaticon-090-pin"></i> 
                                                </div>
                                                <h4 class="aon-con-info-title">Contact Location</h4>
                                                <ul class="aon-con-info-list">
                                                    <li>
                                                        <div class="aon-con-text">
                                                            <strong>No 4/461, 2nd Floor,Valomkottil Towers, Judgemukku, Kakkand, Kochi - 682021</strong>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>	

                        <div class="aon-contact-form-area m-t120 aon-bg-white">
                            <div class="container ">
                                <div class="aon-contact-wrap">
                                    <!--Contact Heading Start--> 
                                    <div class="sf-con-form-title m-b30">
                                        <h3 class="m-b10">Contact Us</h3>
                                        <p>Please share your contact details with Prima PLM, and we will reach out to you*</p>
                                    </div>
                                    <!--Contact Heading End--> 

                                    <!--Contact Form Start-->  
                                    <form class="contact-form" id="contactForm" method="post">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group aon-form-label">
                                                    <input type="text" name="Name" placeholder="Name" class="form-control bottom-line" required>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group aon-form-label">
                                                    <input type="text" name="Company" placeholder="Company" class="form-control bottom-line" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group aon-form-label">
                                                    <input type="email" name="Email" placeholder="Email" class="form-control bottom-line" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group aon-form-label">
                                                    <input type="text" name="Phone" placeholder="Contact Number" class="form-control bottom-line">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group aon-form-label">
                                                    <input type="text" name="City" placeholder="City" class="form-control bottom-line">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group aon-form-label">
                                                    <input type="text" name="Country" placeholder="Country" class="form-control bottom-line">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group aon-form-label">
                                                    <textarea name="message" placeholder="Message" class="form-control bottom-line" rows="4" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="sf-contact-submit-btn">
                                            <button class="site-button site-btn-curve" type="button" onclick="sendMail()">Send Message</button>
                                        </div>
                                    </form>
                                    <div id="form-message" class="form-message"></div>
                                    <div class="loading-spinner" id="loadingSpinner"></div>
                                    <script>
                                        function sendMail() {
                                            var form = document.getElementById('contactForm');
                                            var formData = new FormData(form);
                                            var loadingSpinner = document.getElementById('loadingSpinner');
                                            var formMessage = document.getElementById('form-message');

                                            loadingSpinner.style.display = 'block';
                                            formMessage.innerHTML = '';

                                            var xhr = new XMLHttpRequest();
                                            xhr.open("POST", "send_mail.php", true);
                                            xhr.onload = function () {
                                                loadingSpinner.style.display = 'none';
                                                if (xhr.status === 200) {
                                                    formMessage.innerHTML = "<p style='color:green;'>Message sent successfully!</p>";
                                                    form.reset();
                                                } else {
                                                    formMessage.innerHTML = "<p style='color:red;'>An error occurred. Please try again.</p>";
                                                }
                                            };
                                            xhr.send(formData);
                                        }
                                    </script>
                                    <!--Contact Form End-->
                                </div>
                            </div>
                        </div>

                    </div>
                </div> 
                <!-- Product Section  END -->


            </div>
            <!-- Content END-->

            <?php include_once './i-footer.php'; ?>     

            <!-- BUTTON TOP START -->
            <button class="scroltop"><span class="fa fa-angle-up  relative" id="btn-vibrate"></span></button>



        </div>
        <!-- JAVASCRIPT  FILES ========================================= --> 
        <script  src="js/jquery-3.6.1.min.js"></script><!-- JQUERY.MIN JS -->
        <script  src="js/popper.min.js"></script><!-- POPPER.MIN JS -->
        <script  src="js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->
        <script  src="js/wow.js"></script><!-- WOW JS -->
        <script  src="js/jquery.bootstrap-touchspin.js"></script><!-- FORM JS -->
        <script  src="js/magnific-popup.min.js"></script><!-- MAGNIFIC-POPUP JS -->
        <script  src="js/isotope.pkgd.min.js"></script><!-- isotope-pkgd JS -->	
        <script  src="js/imagesloaded.pkgd.js"></script><!-- isotope-pkgd JS -->		
        <script  src="js/waypoints.min.js"></script><!-- WAYPOINTS JS -->
        <script  src="js/counterup.min.js"></script><!-- COUNTERUP JS -->
        <script  src="js/waypoints-sticky.min.js"></script><!-- STICKY HEADER -->
        <script  src="js/owl.carousel.min.js"></script><!-- OWL  SLIDER  -->
        <script  src="js/theia-sticky-sidebar.js"></script><!-- STICKY SIDEBAR  -->
        <script  src="js/lc_lightbox.lite.js" ></script><!-- IMAGE POPUP -->
        <script  src="js/custom.js"></script><!-- CUSTOM FUCTIONS  -->
        <script  src="js/bootstrap-slider.min.js"></script><!-- Price range slider -->
        <script  src="js/bootstrap-slider.min.js"></script><!-- Price range slider -->

    </body>

</html>
