
<!-- HEADER START -->
<header class="site-header header-style-1 mobile-sider-drawer-menu">
    <!--Top bar section End-->
    <div class="top-bar">  
        <div class="container">
            <div class="top-bar-row">

                <!--Top Bar Left-->
                <div class="top-bar-left"> 
                    <div class="logo-header">
                        <div class="logo-header-inner logo-header-one">
                            <a href="https://primaplm.com/">
                                <img class="p-3" src="images/logo.png" alt="">
                            </a>
                        </div>
                    </div>  
                </div>
                <!--Top Bar Left End-->

                <!--Top Bar Rght-->
                <div class="top-bar-right">
                    <div class="top-bar-one">
                        <div class="top-bar-one-left">
                            <!--<i class="fa fa-map-marker mx-1"></i> Kakkand, Kochi, Kerala-682021-->
                        </div>
                        <div class="top-bar-one-right">
                            <i class="fa fa-envelope mx-1"></i> <a href="mailto:info@primaplm.com">info@primaplm.com</a>
                        </div>								

                    </div> 
                    <div class="top-bar-two">

                        <div class="top-bar-two-right float-end">
                            <div class="top-bar-two-col aon-brand-marketing m-r30">
                                <strong>Streamline PLM with PRIMA's Expertise</strong>  <a href="javascript:void(0);">Free Consultation</a>
                            </div>
                            <div class="top-bar-two-col">
                                <a href="contact-us" class="site-button site-btn-curve aon-get-app-btn">Get Started</a>
                            </div>
                        </div>								

                    </div>
                </div>
                <!--Top Bar Right End-->
            </div>  
        </div>    
    </div>
    <div class="sticky-header main-bar-wraper  navbar-expand-lg">
        <div class="main-bar">  
            <div class="container"> 

                <!-- Mobile Logo -->
                <div class="mobile-logo-left"> 
                    <div class="logo-header">
                        <div class="logo-header-inner logo-header-one">
                            <a href="https://primaplm.com/">
                                <img src="images/logo.png" alt="">
                            </a>
                        </div>
                    </div>  
                </div>

                <!-- NAV Toggle Button -->
                <button id="mobile-side-drawer" data-target=".header-nav" data-toggle="collapse" type="button" class="navbar-toggler collapsed">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar icon-bar-first"></span>
                    <span class="icon-bar icon-bar-two"></span>
                    <span class="icon-bar icon-bar-three"></span>
                </button> 

                <!-- MAIN Vav -->
                <div class="nav-animation header-nav header-nav2 navbar-collapse collapse d-flex">
                    <ul class=" nav navbar-nav">
                        <li class="has-child"><a href="https://primaplm.com/">Home</a></li> 
                        <li class="has-child"><a href="about-us">About Us</a></li> 
                        <li class="has-child"><a href="our-services">Our Services</a></li> 
                        <li class="has-child"><a href="contact-us">Contact Us</a></li> 
                    </ul>
                </div>

                <!-- Header Right Section-->
                <div class="extra-nav header-2-nav">
                    <div class="extra-cell">
                        <ul class="aon-social-icon-3 d-flex">
                            <li><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="javascript:void(0);"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="javascript:void(0);"><i class="fa fa-linkedin-square"></i></a></li>
                        </ul>
                    </div>
                </div>                            
            </div>    
        </div>
    </div>
</header>
<!-- HEADER END -->