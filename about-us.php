
<!DOCTYPE html>
<html lang="zxx">
    <head>

        <!-- META -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- MOBILE SPECIFIC -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">	

        <title>About Us - Prima PLM Solutions</title>
        <meta name="description" content="Learn more about Prima PLM Solutions, our history, mission, and the expert team that provides exceptional PLM services across the globe.">

        <!-- Open Graph / Facebook -->
        <meta property="og:type" content="website">
        <meta property="og:url" content="https://www.primaplm.com/about-us">
        <meta property="og:title" content="About Us - Prima PLM Solutions">
        <meta property="og:description" content="Discover Prima PLM Solutions, our mission, history, and the team dedicated to providing top-notch PLM services worldwide.">
        <meta property="og:image" content="https://www.primaplm.com/images/bg/about_us.webp">

        <!-- Twitter -->
        <meta property="twitter:card" content="summary_large_image">
        <meta property="twitter:url" content="https://www.primaplm.com/about-us">
        <meta property="twitter:title" content="About Us - Prima PLM Solutions">
        <meta property="twitter:description" content="Learn about Prima PLM Solutions' mission, history, and the expert team providing exceptional PLM services worldwide.">
        <meta property="twitter:image" content="https://www.primaplm.com/images/bg/about_us.webp">

        <!-- Organization schema -->
        <script type="application/ld+json">
            {
            "@context": "https://schema.org",
            "@type": "Organization",
            "name": "Prima PLM Solutions",
            "url": "https://www.primaplm.com",
            "logo": "https://www.primaplm.com/images/logo.png",
            "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "+1-800-555-1234",
            "contactType": "Customer Service"
            },
            "sameAs": [
            "https://www.facebook.com/yourprofile",
            "https://www.twitter.com/yourprofile",
            "https://www.linkedin.com/company/yourprofile"
            ]
            }
        </script>

        <!-- Canonical URL -->
        <link rel="canonical" href="https://www.primaplm.com/about-u">

        <link rel="apple-touch-icon" sizes="180x180" href="./images/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="./images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon/favicon-16x16.png">
        <link rel="manifest" href="./images/favicon/site.webmanifest">


        <!-- BOOTSTRAP STYLE SHEET -->
        <link  href="css/bootstrap.min.css" rel="stylesheet">
        <!-- FONTAWESOME STYLE SHEET -->
        <link  href="css/font-awesome.min.css" rel="stylesheet">
        <!-- Feather STYLE SHEET -->
        <link href="css/feather.css" rel="stylesheet">
        <!-- FLATICON STYLE SHEET -->
        <link href="css/flaticon.min.css" rel="stylesheet">
        <!-- WOW ANIMATE STYLE SHEET -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- FONTS CSS STYLE SHEET -->
        <link href="css/font.css" rel="stylesheet">
        <!-- OWL CAROUSEL STYLE SHEET -->
        <link href="css/owl.carousel.min.css" rel="stylesheet">
        <!-- MAGNIFIC POPUP STYLE SHEET -->
        <link href="css/magnific-popup.min.css" rel="stylesheet">     
        <!-- LC LIGHT BOX STYLE SHEET -->
        <link href="css/lc_lightbox.css" rel="stylesheet">   
        <!-- MAIN STYLE SHEET -->
        <link href="css/style.css" rel="stylesheet">

    </head>

    <body>

        <!-- LOADING AREA START ===== -->
        <div class="loading-area" style="display: none;">
            <div class="loading-box"></div>
            <div class="loading-pic">
                <div class="loadind-box" >
                    <span class="let1">l</span>  
                    <span class="let2">o</span>  
                    <span class="let3">a</span>  
                    <span class="let4">d</span>  
                    <span class="let5">i</span>  
                    <span class="let6">n</span>  
                    <span class="let7">g</span>  
                </div>
            </div>
        </div>
        <!-- LOADING AREA  END ====== -->

        <div class="page-wraper">


            <?php include_once './i-header.php'; ?>


            <!-- CONTENT START -->
            <div class="page-content">

                <!-- Inner Page Banner  -->
                <div class="aon-inner-banner-area-about">

                    <div class="aon-inner-banner-row">
                        <div class="overlay"></div>
                        <div class="container aon-inner-banner-container">
                            <div class="aon-inner-banner-title">About Us</div>
                        </div>
                    </div>
                </div>
                <!-- Inner Page Banner End --> 

                <!-- About Section -->
                <div class="aon-about-comp-area p-t120 p-b120 aon-bg-gray">
                    <div class="container">
                        <div class="section-content">
                            <div class="row align-items-center">
                                <!-- Column 1 -->
                                <div class="col-lg-6 wow fadeInDown" data-wow-duration="1000ms">
                                    <div class="aon-pro2-left">
                                        <div class="aon-pro2-top">
                                            <div class="aon-pro2-first">
                                                <img src="images/left-pro-pic.png" alt="Image">
                                            </div>
                                            <div class="aon-pro2-second">
                                                <img src="images/pro-pic22.jpg" alt="">
                                            </div>
                                            <div class="aon-pro2-dot-pic"></div>								
                                        </div>
                                    </div>
                                </div>
                                <!-- Column 2 -->
                                <div class="col-lg-6 wow fadeInDown" data-wow-duration="2000ms">
                                    <div class="aon-pro2-right">
                                        <span class="aon-sub-title">About Us</span>
                                        <h2 class="aon-pro-title">Powering PLM Success with PRIMA</h2>
                                        <div class="aon-about-comp m-b50">
                                            <p><strong>PRIMA PLM</strong> specializes in the implementation and operation of Teamcenter and CAD tools. With many years of experience, we offer expert consulting on engineering and manufacturing processes, ensuring their effective mapping and implementation in Teamcenter.</p>
                                            <p>Our primary focus includes: Management of CAD data, parts data, and manufacturing data and their structures, streamlining processes for lifecycle release, change management and seamless data migration</p>
                                            <p>Our comprehensive approach ensures that your data management processes are efficient, reliable, and tailored to support your engineering and manufacturing needs.</p>
                                        </div>
                                        <div class="line-gray-botton"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- About Section End -->   
            </div>
            <!-- CONTENT END -->

            <?php include_once './i-footer.php'; ?>

            <!-- BUTTON TOP START -->
            <button class="scroltop"><span class="fa fa-angle-up  relative" id="btn-vibrate"></span></button>	
        </div>

        <!-- JAVASCRIPT  FILES ========================================= --> 
        <script  src="js/jquery-3.6.1.min.js"></script><!-- JQUERY.MIN JS -->
        <script  src="js/popper.min.js"></script><!-- POPPER.MIN JS -->
        <script  src="js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->
        <script  src="js/wow.js"></script><!-- WOW JS -->
        <script  src="js/jquery.bootstrap-touchspin.js"></script><!-- FORM JS -->
        <script  src="js/magnific-popup.min.js"></script><!-- MAGNIFIC-POPUP JS -->
        <script  src="js/isotope.pkgd.min.js"></script><!-- isotope-pkgd JS -->	
        <script  src="js/imagesloaded.pkgd.js"></script><!-- isotope-pkgd JS -->		
        <script  src="js/waypoints.min.js"></script><!-- WAYPOINTS JS -->
        <script  src="js/counterup.min.js"></script><!-- COUNTERUP JS -->
        <script  src="js/waypoints-sticky.min.js"></script><!-- STICKY HEADER -->
        <script  src="js/owl.carousel.min.js"></script><!-- OWL  SLIDER  -->
        <script  src="js/theia-sticky-sidebar.js"></script><!-- STICKY SIDEBAR  -->
        <script  src="js/lc_lightbox.lite.js" ></script><!-- IMAGE POPUP -->
        <script  src="js/custom.js"></script><!-- CUSTOM FUCTIONS  -->


    </body>
</html>




